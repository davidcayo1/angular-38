import { Component, OnInit } from '@angular/core';
import { DatosI } from 'src/app/interfaces/datos.interface';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  enviado:boolean;

  constructor() {
    this.enviado = false;
  }

  ngOnInit(): void {
  }
  dato:DatosI ={
    nombre:' ',
    direccion:' '
  };

  enviar():void{
    this.enviado = true;
    console.log('Se envio');
    console.log(this.dato.nombre);
    console.log(this.dato.direccion);
  }
}
